//
//  RegisterViewController.swift
//  Ethernia_IOS
//
//  Created by CNAT on 1/21/20.
//  Copyright © 2020 CNAT. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage

class RegisterViewController:
UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate {
    
    
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var birthdateTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    let birthDatePicker = UIDatePicker ()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ICONO DE CARGANDO EN LA PANTALLA
        activityView.hidesWhenStopped = true
        activityView.isHidden = true
        
        
        //SELECTOR DE FECHA
        birthdateTextField.delegate = self
        birthDatePicker.datePickerMode = .date
        birthDatePicker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
    }
    
    
    
    @IBAction func saveButton(_ sender: Any) {
    
        createUser { userUID in
            //self.uploadImage(userUID : userUID){ imageURL in
            self.saveToFirestore(userUID: userUID){
                
                //self.activityView.stopAnimating()
                
                
                //go to next screen
                self.performSegue(withIdentifier: "registerSucessfully", sender: self)
            }
    
        }
    }
    
    
    func saveToFirestore(userUID: String,
                         completionHandler: @escaping ()->() ) {
        
        let firestore = Firestore.firestore().collection("users").document(userUID)
        activityView.isHidden = false
        activityView.startAnimating()
        
        firestore.setData(
        //["hola":["test"]
        [
            "name" : self.firstnameTextField.text ?? "",
            "lastName" : self.lastnameTextField.text ?? "",
            "email" : self.emailTextField.text ?? "",
            "bithdate" : self.birthdateTextField.text ?? "",
            "phone" : self.phoneTextField.text ?? "",
            
        ]) { (error) in
            
            if error != nil {
                self.activityView.stopAnimating()
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                }
            
        if error == nil {
                      completionHandler()
            self.activityView.stopAnimating()
                    }
        
    }
    }
    
    
    
    func createUser(completionHandler: @escaping (String)->()) {
        let auth = Auth.auth()
        auth.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (authInfo, error) in

            if error != nil { self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }


            if error == nil {
                completionHandler(authInfo!.user.uid)
            }
        }

    }
    
    
    
    
    func showAlert(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(okAlertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
      if textField.tag == birthdateTextField.tag {
        textField.inputView = birthDatePicker
      }
    }
    
    
    @objc func dateValueChanged(_ sender: UIDatePicker) {
        let format = DateFormatter()
        format.dateFormat = "dd - MM - YYY"
        birthdateTextField.text = format.string(from: birthDatePicker.date)
    }
    
    
    @IBAction func cancelButton(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

//
//  ViewController.swift
//  Ethernia_IOS
//
//  Created by CNAT on 1/14/20.
//  Copyright © 2020 CNAT. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    
    
    @IBAction func logginButton(_ sender: Any) {
        
        
        guard let email = emailTextField.text,
                     let password = passwordTextField.text else {
                             return
                         }
                         Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                                 //print("RESULT: \(result)")
                             //print("ERROR: \(error)")
                             
                             if error != nil {
                                 self.presentAlertWith(title: "Error", message: error?.localizedDescription ?? "Ups! An error ocurred")
                             } else{
                                 self.performSegue(withIdentifier: "home_to_login", sender: self) //union entre pantallas
                             }
                         }
        
    }
    
    
    
    private func presentAlertWith(title: String, message: String){
                let alertControlller = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                let okAlertAcction = UIAlertAction(title: "OK", style: .default) {
                    _ in
                    self.emailTextField.text = ""
                    self.passwordTextField.text = ""
                    
                    self.emailTextField.becomeFirstResponder()
                    
                }
                
                alertControlller.addAction(okAlertAcction)
                
                present(alertControlller, animated: true, completion: nil)
            }
    
    
    
    @IBAction func registerButton(_ sender: Any) {
    }
    
}

